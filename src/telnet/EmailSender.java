package telnet;
import java.net.*;
import java.io.*;


public class EmailSender implements AutoCloseable {
	private Socket s;
	private OutputStream out;

    /*
     * Constructor opens Socket to host/port. If the Socket throws an exception during opening,
     * the exception is not handled in the constructor.
     */
    public EmailSender(String host, int port) throws UnknownHostException, IOException {
		s = new Socket(host, port);
		out = s.getOutputStream();
    }
    
    /*
     * sends email from an email address to an email address with some subject and text.
     * If the Socket throws an exception during sending, the exception is not handled by this method.
     */
    public void send(String from, String to, String subject, String text) throws IOException, InterruptedException {
    	out.write(String.format("MAIL FROM: %s\r\nRCPT TO: %s\r\nDATA\r\nSubject: %s\r\n%s\r\n.\r\n", from, to, subject, text).getBytes());
		Thread.sleep(10000);
    }
   
    /*
     * sends QUIT and closes the socket
     */
    public void close() throws IOException {
        out.write("QUIT".getBytes());
        s.close();
    }
}
